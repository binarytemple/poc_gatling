Introduction
============

This is a small project to research Gatling load testing tool.

Pre-requisites
--------------
Maven

Creating from scratch (unnecessary)
-----------------------------------

If you want to start from scratch 

Add Gatling repository to your ~/.m2/settings.xml

     <settings xmlns="http://maven.apache.org/SETTINGS/1.1.0"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.1.0
                       http://maven.apache.org/xsd/settings-1.1.0.xsd">
     <profiles>
         <profile>
             <id>myProfile</id>
             <activation>
                 <activeByDefault>true</activeByDefault>
             </activation>
             <repositories>
                 <repository>
                     <id>repository.excilys.com</id>
                     <url>http://repository.excilys.com/content/repositories/releases</url>
                     <releases>
                         <enabled>true</enabled>
                     </releases>
                     <snapshots>
                         <enabled>false</enabled>
                     </snapshots>
                 </repository>
             </repositories>
         </profile>
     </profiles>
     </settings>


Run mvn archetype:generate

Chose the gatling archetype (653)

Ok, now the sample project is generated.

Conclusion
----------
Run Engine.scala to execute the sample test.

