package hunt

import com.excilys.ebi.gatling.core.Predef._
import com.excilys.ebi.gatling.http.Predef._
import com.excilys.ebi.gatling.jdbc.Predef._
import com.excilys.ebi.gatling.http.Headers.Names._
import akka.util.duration._
import bootstrap._

class BinarytempleSimulation extends Simulation {
  val httpConf = httpConfig.baseURL("http://www.binarytemple.co.uk")
  val scn = scenario("My scenario")
    .exec(http("Index page")
    .get("/")
    .queryParam("q", "gatling tool"))
  setUp(scn.users(10).ramp(10).protocolConfig(httpConf))
}