import com.excilys.ebi.gatling.recorder.config.RecorderOptions
import com.excilys.ebi.gatling.recorder.controller.RecorderController

object Recorder extends App {
  RecorderController(
    new RecorderOptions(localPort = None, localPortSsl = None, proxyHost = None, proxyPort = None, proxyPortSsl = None,
      outputFolder = Some(IDEPathHelper.recorderOutputDirectory.toString()),
      requestBodiesFolder = Some(IDEPathHelper.requestBodiesDirectory.toString()), simulationClassName = None,
      simulationPackage = Some("hunt"), encoding = None, followRedirect = None, automaticReferer = None))
}